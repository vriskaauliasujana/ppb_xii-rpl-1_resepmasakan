class Resep {
  String name, tutorial, image;

  Resep(
      {required this.name,
      required this.tutorial,
      required this.image});
}

List<Resep> dataResep = [
  Resep(
      name: 'Martabak Mini',
      tutorial: 'Bahan :\n• 1 butir telur\n• 1 sendok teh fermipan\n• 250 gram tepung terigu\n• 1 sendok teh baking powder\n• Sendok makan gula pasir\n• Mentega secukupnya\n• Taburan coklat secukupnya\n• Taburan keju secukupnya\n\n Cara Membuat :\n1. Siapkan wadah atau mangkuk berukuran besar\n2. Kocok telur dan gula hingga bercampur\n3. Masukkan tepung terigu, fermipan, baking powder, dan garam\n4. Campurkan dan terus diaduk\n5. Jika sudah tercampur, diamkan sejenak selama 1-2 jam\n6. Siapkan cetakan martabak mini dan olesi menggunakan mentega lalu Tuangkan adonan ke cetakan\n7. Tekan bagian atasnya agar mendapatkan pinggiran martabak Jika adonan mulai terlihat berlubang tambahkan gula\n8. Angkat saat martabak telah matang\n9. Tambahkan taburan coklat dan keju',
      image: 'assets/martabakmini.jpg'),
  Resep(
      name: 'Singkong Keju',
      tutorial: 'Bahan:\n• Singkong 1,5 kg\n• Air dingin 1.5 liter (untuk merendam singkong)\n• Keju cheddar secukupnya, parut\n\n Bumbu Halus:\n• Bawang Putih 1 siung\n• Ketumbar 3 sendok teh\n• Garam 3 sendok teh \n\nCara Membuat :\n1. Pertama kupas singkong kemudian cuci hingga bersih lalu potong-potong dengan ukuran agak besar.\n2. Selanjutnya siapkan kukusan lalu kukus singkong selama 20-30 menit hingga empuk lalu angkat dan sisihkan.\n3. Setelah itu siapkan wadah kemudian masukkan air dingin atau air es serta bumbu halus lalu aduk hingga tercampur rata.\n4. Selanjutnya masukkan singkong yang telah dikukus kedalam wadah air dingin dan diamkan singkong selama 1 jam sampai merekah, lalu angkat dan tiriskan\n5. Langkah selanjutnya panaskan minyak dalam jumlah banyak kemudian goreng singkong hingga berwarna kuning kecoklatan lalu angkat dan tiriskan.\n6. Terakhir sebelum disajikan taburi dengan toping keju parut.',
      image: 'assets/singkongkeju.jpg'),
  Resep(
      name: 'Salad Sayur',
      tutorial: 'Bahan:\n• 3-4 helai Kol Merah, iris tipis\n• 1 buah wortel potong korek\n• 5 buah tomat ceri\n• 1 buah timun ukuran kecil, iris\n• 3 sendok makan jagung manis pipil rebus\n• 150 gram selada, potong/sobek sesuai selera\n• 1 buah telur rebus matang, potong-potong\n• 1 buah bawang bombay merah, iris tipis\n• Maestro Thousand Island Salad Dressing\n• Air dingin matang\n• Garam, gula, air perasan jeruk nipis\n\nCara Membuat :\n1. Rendam selama 15 menit semua bahan salad di air dingin matang kecuali telur rebus. Deri 1 sendok makan garam, 1 sendok makan gula pasir, dan 2 sendok makan air peras jeruk nipis\n2. Rendaman bahan salad sayur sebaiknya diletakkan di kulkas agar dingin\n3. Keluarkan dari kulkas setelah 15 menit, tiriskan air rendamannya\n4. Siapkan saus Thousand Island. Tuangkan di atas isian salad sayur, lalu mix salad sampai tercampur ke seluruh isiannya.\n5. Sajikan',
      image: 'assets/saladsayur.jpg'),
];
